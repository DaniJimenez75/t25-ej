DROP TABLE IF EXISTS `Empleados`;
DROP table IF EXISTS `Departamentos`;

CREATE TABLE `Departamentos` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `presupuesto` int DEFAULT NULL,
  PRIMARY KEY (`codigo`)
);


CREATE TABLE `Empleados` (
  `dni` varchar(8) NOT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `apellido` varchar(250) DEFAULT NULL,
  `departamentos` int DEFAULT NULL,
  PRIMARY KEY (`dni`),
  CONSTRAINT `Empleados_1` FOREIGN KEY (`departamentos`) REFERENCES `Departamentos` (`codigo`)
);



INSERT INTO `Departamentos` VALUES (1,'Departamento 1',2500),(2,'Departamento 2',3000);
INSERT INTO `Empleados` VALUES ('12345678','Daniel','Jiménez',1),('87654321','Alejandro','Largo',2);


