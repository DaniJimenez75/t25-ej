package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.dto.Empleados;


public interface IEmpleadosService {
	
	public List<Empleados> listarEmpleados(); //Listar All 
	
	public Empleados guardarEmpleado(Empleados empleado);	//Guarda un cliente CREATE
	
	public Empleados empleadoXDNI(String dni); //Leer datos de un cliente READ
	
	public Empleados actualizarEmpleado(Empleados empleado); //Actualiza datos del cliente UPDATE
	
	public void eliminarEmpleado(String dni);// Elimina el cliente DELETE

}
