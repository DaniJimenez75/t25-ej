package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="empleados")//en caso que la tabla sea diferente
public class Empleados {

	//Atributos de entidad cliente
			@Id
			@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
			@Column(name = "dni")
			private String dni;
			@Column(name = "nombre")
			private String nombre;
			@Column(name = "apellidos")
			private String apellidos;
			
			
			@ManyToOne
		    @JoinColumn(name="departamento")
		    private Departamentos departamentos;
			
			//Constructores
			
			public Empleados() {
				
			}
			
			public Empleados(String dni, String nombre, String apellidos, Departamentos departamentos) {
				this.dni = dni;
				this.nombre = nombre;
				this.apellidos = apellidos;
				this.departamentos = departamentos;
			}
			
			// getter y setter
			/**
			 * @return the id
			 */
			public String getDni() {
				return dni;
			}


			/**
			 * @param id the id to set
			 */
			public void setDni(String dni) {
				this.dni = dni;
			}
			
			
			public String getNombre() {
				return nombre;
			}


			/**
			 * @param id the id to set
			 */
			public void setNombre(String nombre) {
				this.nombre = nombre;
			}
			
			
			public String getApellidos() {
				return apellidos;
			}


			/**
			 * @param id the id to set
			 */
			public void setApellidos(String apellidos) {
				this.apellidos = apellidos;
			}
			

			public Departamentos getDepartamentos() {
				return departamentos;
			}


			/**
			 * @param id the id to set
			 */
			public void setDepartamentos(Departamentos departamentos) {
				this.departamentos = departamentos;
			}


			@Override
			public String toString() {
				return "Empleados [dni=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos + ", departamentos="
						+ departamentos + "]";
			}
			
			
			
	
}
