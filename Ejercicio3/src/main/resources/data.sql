DROP table IF EXISTS `Cajas`;
DROP TABLE IF EXISTS `Almacenes`;


CREATE TABLE `Almacenes` (
  `codigo` int NOT NULL AUTO_INCREMENT,
  `lugar` varchar(100) DEFAULT NULL,
  `capacidad` int DEFAULT NULL,
  PRIMARY KEY (`codigo`)
);


CREATE TABLE `Cajas` (
  `numreferencia` char(5) NOT NULL,
  `contenido` varchar(100) DEFAULT NULL,
  `valor` int DEFAULT NULL,
  `almacen` int DEFAULT NULL,
  PRIMARY KEY (`numReferencia`),
  CONSTRAINT `Almacenes_1` FOREIGN KEY (`almacen`) REFERENCES `Almacenes` (`codigo`)
);



INSERT INTO `Almacenes` VALUES (1,'Tarragona',1000),(2,'Barcelona',700);
INSERT INTO `Cajas` VALUES ('1A','Bebidas','50',1),('2A','Carne','50',2);


