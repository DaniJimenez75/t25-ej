package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Almacenes;
import com.example.demo.service.AlmacenesServiceImpl;



@RestController
@RequestMapping("/api")
public class AlmacenesController {
	@Autowired
	AlmacenesServiceImpl almacenServideImpl;
	
	@GetMapping("/almacenes")
	public List<Almacenes> listarAlmacenes(){
		return almacenServideImpl.listarAlmacenes();
	}
	
	@PostMapping("/almacenes")
	public Almacenes salvarAlmacen(@RequestBody Almacenes almacen) {
		
		return almacenServideImpl.guardarAlmacen(almacen);
	}
	
	@GetMapping("/almacenes/{codigo}")
	public Almacenes almacenXCodigo(@PathVariable(name="codigo") int codigo) {
		
		Almacenes departamento_xCodigo= new Almacenes();
		
		departamento_xCodigo=almacenServideImpl.almacenXCodigo(codigo);
		
		System.out.println("Departamento XCodigo: "+departamento_xCodigo);
		
		return departamento_xCodigo;
	}
	
	@PutMapping("/almacenes/{codigo}")
	public Almacenes actualizarAlmacen(@PathVariable(name="codigo")int codigo,@RequestBody Almacenes almacen) {
		
		Almacenes almacen_seleccionado= new Almacenes();
		Almacenes almacen_actualizado= new Almacenes();
		
		almacen_seleccionado= almacenServideImpl.almacenXCodigo(codigo);
		
		almacen_seleccionado.setLugar(almacen.getLugar());
		almacen_seleccionado.setCapacidad(almacen.getCapacidad());

		
		almacen_actualizado = almacenServideImpl.actualizarAlmacen(almacen_seleccionado);
		
		System.out.println("El almacen actualizado es: "+ almacen_actualizado);
		
		return almacen_actualizado;
	}
	
	@DeleteMapping("/almacenes/{codigo}")
	public void eleiminarAlmacen(@PathVariable(name="codigo")int codigo) {
		almacenServideImpl.eliminarAlmacen(codigo);
	}
	
	
}
