package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Cajas;
import com.example.demo.service.CajasServiceImpl;


@RestController
@RequestMapping("/api")
public class CajasController {
	@Autowired
	CajasServiceImpl cajaServideImpl;
	
	@GetMapping("/cajas")
	public List<Cajas> listarCajas(){
		return cajaServideImpl.listarCajas();
	}
	
	@PostMapping("/cajas")
	public Cajas salvarCaja(@RequestBody Cajas caja) {
		
		return cajaServideImpl.guardarCaja(caja);
	}
	
	@GetMapping("/cajas/{numReferencia}")
	public Cajas cajaXNumReferencia(@PathVariable(name="numReferencia") String numReferencia) {
		
		Cajas caja_xnumReferencia= new Cajas();
		
		caja_xnumReferencia=cajaServideImpl.cajaXNumReferencia(numReferencia);
		
		System.out.println("Caja X NumReferencia: "+caja_xnumReferencia);
		
		return caja_xnumReferencia;
	}
	
	@PutMapping("/cajas/{numReferencia}")
	public Cajas actualizarCaja(@PathVariable(name="numReferencia")String numReferencia,@RequestBody Cajas caja) {
		
		Cajas caja_seleccionada= new Cajas();
		Cajas caja_actualizada= new Cajas();
		
		caja_seleccionada= cajaServideImpl.cajaXNumReferencia(numReferencia);
		
		caja_seleccionada.setContenido(caja.getContenido());
		caja_seleccionada.setValor(caja.getValor());
		caja_seleccionada.setAlmacenes(caja.getAlmacenes());

		
		caja_actualizada = cajaServideImpl.actualizarCaja(caja_seleccionada);
		
		System.out.println("La caja actualizada es: "+ caja_actualizada);
		
		return caja_actualizada;
	}
	
	@DeleteMapping("/cajas/{numReferencia}")
	public void eliminarCaja(@PathVariable(name="numReferencia")String numReferencia) {
		cajaServideImpl.eliminarCaja(numReferencia);
	}
}
