package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.dto.Almacenes;



public interface IAlmacenesService {
	public List<Almacenes> listarAlmacenes(); //Listar All 
	
	public Almacenes guardarAlmacen(Almacenes almacen);	//Guarda un cliente CREATE
	
	public Almacenes almacenXCodigo(int codigo); //Leer datos de un cliente READ
	
	public Almacenes actualizarAlmacen(Almacenes almacen); //Actualiza datos del cliente UPDATE
	
	public void eliminarAlmacen(int codigo);// Elimina el cliente DELETE
}
