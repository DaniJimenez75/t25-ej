package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Articulo;

public interface IArticuloService {

	//Metodos del CRUD
	public List<Articulo> listarArticulos();
	
	public Articulo guardarArticulo(Articulo articulo);
	
	public Articulo articuloXID(int id);
	
	public Articulo actualizarArticulo(Articulo articulo_seleccionado);
	
	public void eliminarArticulo(int id);
}
