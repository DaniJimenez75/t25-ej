DROP TABLE IF EXISTS `articulos`;
DROP TABLE IF EXISTS `fabricantes`;


CREATE TABLE `fabricantes` (
  `codigo` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
);

CREATE TABLE `articulos` (
  `codigo` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `precio` int DEFAULT NULL,
  `fabricante` int DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  CONSTRAINT `articulos_fk` FOREIGN KEY (`fabricante`) REFERENCES `fabricantes` (`codigo`)
);

INSERT INTO `fabricantes` VALUES (1,'Manuel'),(2,'Marc');
INSERT INTO `articulos` VALUES (1,'PC',1000,2),(2,'RAM',60,1);




