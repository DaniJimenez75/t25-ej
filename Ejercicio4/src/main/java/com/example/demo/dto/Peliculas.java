package com.example.demo.dto;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;




@Entity
@Table(name="Peliculas")//en caso que la tabla sea diferente
public class Peliculas {

	//Atributos de entidad cliente
			@Id
			@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
			@Column(name = "codigo")
			private int codigo;
			@Column(name = "nombre")
			private String nombre;
			@Column(name = "calificacionedad")
			private int calificacionEdad;
			
			
			@OneToMany
		    @JoinColumn(name="codigo")
			private List<Salas> sala;


			public Peliculas(int codigo, String nombre, int calificacionEdad, List<Salas> sala) {
				this.codigo = codigo;
				this.nombre = nombre;
				this.calificacionEdad = calificacionEdad;
				this.sala = sala;
			}
			
			public Peliculas() {

			}

			public int getCodigo() {
				return codigo;
			}

			public void setCodigo(int codigo) {
				this.codigo = codigo;
			}

			public String getNombre() {
				return nombre;
			}

			public void setNombre(String nombre) {
				this.nombre = nombre;
			}

			public int getCalificacionEdad() {
				return calificacionEdad;
			}

			public void setCalificacionEdad(int calificacionEdad) {
				this.calificacionEdad = calificacionEdad;
			}
	

			
			

			@JsonIgnore
			@OneToMany(fetch = FetchType.LAZY, mappedBy = "Salas")
			public List<Salas> getSala() {
				return sala;
			}

			public void setSala(List<Salas> sala) {
				this.sala = sala;
			}

			@Override
			public String toString() {
				return "Peliculas [codigo=" + codigo + ", nombre=" + nombre + ", calificacionEdad=" + calificacionEdad
						+  "]";
			}

			
			
			
			

			

			
			
			
			
			
			
			
			
	
}
