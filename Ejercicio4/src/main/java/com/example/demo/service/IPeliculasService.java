package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.dto.Peliculas;


public interface IPeliculasService {
	
	public List<Peliculas> listarPeliculas(); //Listar All 
	
	public Peliculas guardarPeliculas(Peliculas peliculas);	//Guarda un cliente CREATE
	
	public Peliculas peliculasXCodigo(int codigo); //Leer datos de un cliente READ
	
	public Peliculas actualizarPeliculas(Peliculas peliculas); //Actualiza datos del cliente UPDATE
	
	public void eliminarPeliculas(int codigo);// Elimina el cliente DELETE

}
