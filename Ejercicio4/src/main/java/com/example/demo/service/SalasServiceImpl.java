package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.ISalasDAO;
import com.example.demo.dto.Salas;


@Service
public class SalasServiceImpl implements ISalasService {

	//Utilizamos los metodos de la interface ISalasDAO, es como si instaciaramos.
	@Autowired
	ISalasDAO iSalasDAO;
	
	@Override
	public List<Salas> listarSalas() {
		
		return iSalasDAO.findAll();
	}

	@Override
	public Salas guardarSalas(Salas salas) {
		
		return iSalasDAO.save(salas);
	}

	@Override
	public Salas salasXCodigo(int codigo) {
		
		return iSalasDAO.findById(codigo).get();
	}

	@Override
	public Salas actualizarSalas(Salas salas) {
		
		return iSalasDAO.save(salas);
	}

	@Override
	public void eliminarSalas(int codigo) {
		
		iSalasDAO.deleteById(codigo);
		
	}
}
