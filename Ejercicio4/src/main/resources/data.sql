DROP TABLE IF EXISTS `Salas`;
DROP TABLE IF EXISTS `Peliculas`;




CREATE TABLE `Peliculas` (
  `codigo` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `calificacionEdad` int DEFAULT NULL,
  PRIMARY KEY (`codigo`)
);

CREATE TABLE `Salas` (
  `codigo` int NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `pelicula` int DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  CONSTRAINT `sala_fk` FOREIGN KEY (`pelicula`) REFERENCES `Peliculas` (`codigo`)
);

INSERT INTO `Peliculas` VALUES (1,'Peli 1',18),(2,'Peli 2',15);
INSERT INTO `Salas` VALUES (1,'Sala 1',1),(2,'Sala 2',2);


